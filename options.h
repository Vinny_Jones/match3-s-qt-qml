#ifndef OPTIONS_H
#define OPTIONS_H

#include <QFile>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QDebug>

#define		DEFAULT_FILE "default.json"

class Options
{
		friend class Logic;
//		static const char		*defaultFile = "default.json";	??
		static const int		max_types = 10;
		static const int		min_types = 4;
		static const int		max_width = 25;
		static const int		min_width = 5;
		static const int		max_height = 15;
		static const int		min_height = 5;
	public:
		Options();
		~Options();
		void		saveOptions();
	public slots:
		void		setOptions(QString filename);
		void		loadOptions();
	private:
		QFile		m_optionsFile;
		int			m_width;
		int			m_height;
		QList<int>	m_types;
		int			m_elementScore;
		int			m_minScore;
		int			m_maxMoves;
		QStringList	m_images;
};

#endif // OPTIONS_H
