#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "logic.h"

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;

	Options		gameOptions;
	Logic		gameLogic(&gameOptions);

	engine.rootContext()->setContextProperty("logic", &gameLogic);
	engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

	return (app.exec());
}
