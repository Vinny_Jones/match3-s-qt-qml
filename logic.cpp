#include "logic.h"

#include <iostream>

Logic::Logic(Options *options, QObject *parent) : QAbstractListModel(parent), m_options(options)
{
	newGame();
	srand(time(NULL));
}

Logic::~Logic()
{
}

int			Logic::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return (totalElems());
}

QVariant	Logic::data(const QModelIndex &index, int role) const
{
	role = m_gameField[index.row()];
	if (role < 0)
		return (-1);
	return (QString(m_options->m_images[role]));
}

QHash<int, QByteArray>	Logic::roleNames() const
{
	QHash<int, QByteArray>	roles;

	roles.insert(Qt::UserRole + 1, "figureImg");
	return roles;
}

inline int  Logic::rows() const
{
	return (m_options->m_height + 1);
}

inline int	Logic::columns() const
{
    return (m_options->m_width);
}

inline int	Logic::totalElems() const
{
	return (rows() * columns());
}

int			Logic::getMinScore() const
{
	return (m_options->m_minScore);
}

int			Logic::getMaxMoves() const
{
	return (m_options->m_maxMoves);
}

int			Logic::getTypeCount() const
{
	return (m_options->m_types.size());
}

inline bool Logic::isValid(int col, int row)
{
	return (col >= 0 && col < columns() && row >= 0 && row < rows());
}

unsigned int	Logic::movesLeft()
{
	return (m_options->m_maxMoves - m_moves);
}

void		Logic::fillEmpty()
{
	std::vector<bool>    colMark(columns(), true);

	for (int i = 0; (i = m_gameField.indexOf(-1, i)) >= 0 && i < m_gameField.size(); i++)
	{
		int col = i % columns();
		if (!colMark[col])
			continue ;
		colMark[col] = false;
		deleteElement(i);
	}
}

void		Logic::checkPossibleMove()
{
	QList<int>	gameFieldCopy = m_gameField;
	QList<int>	toDelTemp;
	QList<int>	directions;

	directions << 1;
	directions << -1;
	directions << columns();
	directions << -columns();

	this->blockSignals(true);
	for (int i = columns(); i < gameFieldCopy.size(); i++)
	{
		for (int j = 0; j < directions.size(); j++)
		{
			swapElement(i, i + directions[j], &gameFieldCopy);
			if (findMatchLine(gameFieldCopy, toDelTemp, i, 1) || findMatchLine(gameFieldCopy, toDelTemp, i, columns()))
			{
				this->blockSignals(false);
				emit hintItems(i, i + directions[j]);
				return ;
			}
			swapElement(i, i + directions[j], &gameFieldCopy);
		}
	}
	this->blockSignals(false);
	emit movesEnded();
}


bool		Logic::findMatch()
{
	bool		match1, match2;
	QList<int>	toDel;

	for (int i = columns(); i < m_gameField.size(); i++)
	{
		if (toDel.indexOf(i) >= 0)
			continue ;
		toDel << i;
		match1 = findMatchLine(m_gameField, toDel, i, 1);
		match2 = (match1 ? true : findMatchLine(m_gameField, toDel, i, columns()));
		if (!match1 && !match2)
			toDel.pop_back();
	}

	for (int i = 0; i < toDel.size(); i++)
	{
		m_gameField[toDel[i]] = -1;
		m_score += m_options->m_elementScore;
		emit scoreChanged();
		emit elementDeleted(toDel[i]);
	}

	return (toDel.size());
}

bool	Logic::findMatchLine(QList<int> &gameField, QList<int> &toDel, int index, int direction)
{
	QList<int>	temp;
	int			i;

	for (i = direction; checkMatch(gameField, index, i); i += direction)
		temp << index + i;
	for (i = -direction; checkMatch(gameField, index, i); i -= direction)
		temp << index + i;

	if (temp.size() < 2)
		return (false);
	for (i = 0; i < temp.size(); i++)
	{
		if (toDel.indexOf(temp[i]) >= 0)
			continue ;
		toDel << temp[i];
		findMatchLine(gameField, toDel, temp[i], (direction == 1 ? columns() : 1));
	}
	return (true);
}


bool		Logic::checkMatch(QList<int> &gameField, int index, int direction)
{
	int     col = index % columns();
	int     row = index / columns();
	int		destCol = (index + direction) % columns();
	int		destRow = (index + direction) / columns();

	if (!isValid(col, row) || !isValid(destCol, destRow) || !destRow || !row)
		return (false);
	if (gameField[index] == gameField[index + direction] && (col == destCol || row == destRow))
		return (true);
	return (false);
}


void		Logic::deleteElement(int index)
{
    int     col = index % columns();
	int     row = index / columns();

    if (!isValid(col, row))
        return ;
	QModelIndex	parent;

	for (int i = row; i > 0; i--)
    {
        int current = i * columns() + col;
        int prev = (i - 1) * columns() + col;
        beginMoveRows(parent, prev, prev, parent, current);
        m_gameField.swap(prev, current);
        endMoveRows();
    }
    beginInsertRows(parent, col, col);
    endInsertRows();
    beginRemoveRows(parent, col + row * columns() + 1, col + row * columns() + 1);
    endRemoveRows();

    m_gameField[col] = rand() % m_options->m_types.size();
	emit dataChanged(createIndex(col, 0), createIndex(col, 0));
}


/* public slots */
void    Logic::newGame()
{
	m_gameField.clear();
	for (int i = 0; i < totalElems(); i++)
	{
		m_gameField << ((i < columns()) ? rand() % m_options->m_types.size() : -1);
		emit dataChanged(createIndex(i, 0), createIndex(i, 0));
	}
	m_lastMove = QPair<int, int>(-1, -1);
	m_score = 0;
	m_moves = 0;
	emit scoreChanged();
	emit movesLeftChanged();
}

void	Logic::clearBoard()
{
	for (int i = columns(); i < totalElems(); i++)
	{
		m_gameField[i] = -1;
		emit dataChanged(createIndex(i, 0), createIndex(i, 0));
	}
}

void	Logic::addMove()
{
	++m_moves;
	emit movesLeftChanged();
}

void		Logic::newOptionsFile(QString filename)
{
	m_options->setOptions(filename);
	QProcess::startDetached(QCoreApplication::applicationFilePath());
	exit(0);
}

void        Logic::swapElement(int first, int second, QList<int> *gameField)
{
	int firstCol = first % columns();
	int firstRow = first / columns();
	int secondCol = second % columns();
	int secondRow = second / columns();

	if (first == second || !isValid(firstCol, firstRow) || !isValid(secondCol, secondRow) || !firstRow || !secondRow)
		return ;
	if (abs(first - second) != 1 && abs(first - second) != columns())
		return ;
	if (firstCol != secondCol && firstRow != secondRow)
		return ;

	if (m_lastMove.first < 0 && !gameField)
		addMove();

	QModelIndex		parent;
	gameField = (gameField) ? gameField : &m_gameField;
	gameField->swap(first, second);

	beginMoveRows(parent, std::max(first, second), std::max(first, second), parent, std::min(first, second));
	if (abs(first - second) > 1)
	{
			beginMoveRows(parent, std::min(first, second), std::min(first, second), parent, std::max(first, second));
			endMoveRows();
	}
	endMoveRows();
	m_lastMove = QPair<int, int>(first, second);
}

void        Logic::checkBoard()
{
	if (m_gameField.indexOf(-1, 0) >= 0)
		return fillEmpty();

	if (findMatch())
		checkBoard();
	else if (m_moves == m_options->m_maxMoves)
		emit gameFinished(m_score, m_options->m_minScore);
	else if (m_lastMove.first >= 0)
		swapElement(m_lastMove.first, m_lastMove.second);
	else
		checkPossibleMove();
	m_lastMove = QPair<int, int>(-1, -1);
}
