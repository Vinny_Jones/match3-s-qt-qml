#include "options.h"

Options::Options() : m_optionsFile(DEFAULT_FILE)
{
	m_images << "qrc:/images/android.png";
	m_images << "qrc:/images/apple.png";
	m_images << "qrc:/images/chrome.png";
	m_images << "qrc:/images/firefox.png";
	m_images << "qrc:/images/github.png";
	m_images << "qrc:/images/facebook.png";
	m_images << "qrc:/images/twitter.png";
	m_images << "qrc:/images/youtube.png";
	m_images << "qrc:/images/soundcloud.png";
	m_images << "qrc:/images/drive.png";
	loadOptions();
}

Options::~Options()
{
}

void	Options::loadOptions()
{
	if (!m_optionsFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		m_width = 10;
		m_height = 10;
		m_types = {0, 1, 2, 3, 4};
		m_elementScore = 60;
		m_minScore = 10000;
		m_maxMoves = 20;
		saveOptions();
		return ;
	}

	QString			input = m_optionsFile.readAll();
	QJsonObject		objJSON = QJsonDocument::fromJson(input.toUtf8()).object();

	int	temp;
	m_optionsFile.close();
	temp = objJSON.value(QString("width")).toInt();
	if (temp < min_width)
		m_width = min_width;
	else if (temp > max_width)
		m_width = max_width;
	else
		m_width = temp;
	temp = objJSON.value(QString("height")).toInt();
	if (temp < min_height)
		m_height = min_height;
	else if (temp > max_height)
		m_height = max_height;
	else
		m_height = temp;

	m_types.clear();
	QJsonArray		typesArray = objJSON.value(QString("types")).toArray();
	for(int i = 0; (i < typesArray.size() && i < max_types) || i < min_types; i++)
		m_types << i;
	m_elementScore = objJSON.value(QString("elementScore")).toInt();
	m_elementScore = (m_elementScore <= 0) ? 1 : m_elementScore;
	m_minScore = objJSON.value(QString("minScore")).toInt();
	m_minScore = (m_minScore <= 0) ? 1 : m_minScore;
	m_maxMoves = objJSON.value(QString("maxMoves")).toInt();
	m_maxMoves = (m_maxMoves <= 0) ? 1 : m_maxMoves;
	if (m_optionsFile.fileName() != DEFAULT_FILE)
		saveOptions();
}

void	Options::setOptions(QString filename)
{
	QString		oldFileName = m_optionsFile.fileName();
	m_optionsFile.setFileName(filename);
	if (!m_optionsFile.exists())
		m_optionsFile.setFileName(oldFileName);
	loadOptions();
}

void	Options::saveOptions()
{
	m_optionsFile.setFileName(DEFAULT_FILE);
	if (!m_optionsFile.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
		return ;
	QJsonObject		output;
	output["width"] = m_width;
	output["height"] = m_height;
	QJsonArray		types;
	for (int i = 0; i < m_types.size(); i++)
		types << m_types[i];
	output["types"] = types;
	output["elementScore"] = m_elementScore;
	output["minScore"] = m_minScore;
	output["maxMoves"] = m_maxMoves;

	QByteArray bytes = QJsonDocument(output).toJson();
	m_optionsFile.write(bytes);
	m_optionsFile.close();
}
