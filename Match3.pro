TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += logic.cpp \
	main.cpp \
	options.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

HEADERS += \
    options.h \
    logic.h
