#ifndef LOGIC_H
#define LOGIC_H

#include <QDebug>

#include "options.h"
#include <QAbstractListModel>
#include <QCoreApplication>
#include <QProcess>

class Logic : public QAbstractListModel
{
		Q_OBJECT
        Q_PROPERTY(int totalRows READ rows CONSTANT)
        Q_PROPERTY(int totalCols READ columns CONSTANT)
		Q_PROPERTY(int minScore READ getMinScore CONSTANT)
		Q_PROPERTY(int maxMoves READ getMaxMoves CONSTANT)
		Q_PROPERTY(int typeCount READ getTypeCount CONSTANT)
		Q_PROPERTY(unsigned int score MEMBER m_score NOTIFY scoreChanged)
		Q_PROPERTY(unsigned int movesLeft READ movesLeft NOTIFY movesLeftChanged)
	public:
		Logic(Options *options, QObject *parent = 0);
		~Logic();
		int						rowCount(const QModelIndex &parent) const override;
		QVariant				data(const QModelIndex &index, int role) const override;
		QHash<int, QByteArray>	roleNames() const override;
        inline int      		rows() const;
        inline int      		columns() const;
		inline int      		totalElems() const;
		int						getMinScore() const;
		int						getMaxMoves() const;
		int						getTypeCount() const;
		inline bool             isValid(int col, int row);
		unsigned int			movesLeft();
		void					fillEmpty();
		void					checkPossibleMove();
		bool					findMatch();
		bool					findMatchLine(QList<int> &gameField, QList<int> &toDel, int index, int direction);
		bool					checkMatch(QList<int> &gameField, int index, int direction);
		void					deleteElement(int index);

	public slots:
		void                    newGame();
		void					clearBoard();
		void					addMove();
		void					newOptionsFile(QString filename);
		void                    swapElement(int first, int second, QList<int> *gameField = nullptr);
		void                    checkBoard();

	signals:
		void					elementDeleted(int index);
		void					scoreChanged();
		void					movesEnded();
		void					gameFinished(int score, int minScore);
		void					movesLeftChanged();
		void					hintItems(int first, int second);

    private:
        Options                 *m_options;
        unsigned int            m_score;
		unsigned int			m_moves;
		QList<int>              m_gameField;
		QPair<int, int>			m_lastMove;
};

#endif // LOGIC_H
