import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Rectangle {
	anchors.top: parent.top
	anchors.left: parent.left
	height: 120
	width: parent.width
	color: "lightyellow"
	z:1

	CustomButton {
		id:newGameButton
		z:1
		anchors.left: parent.left
		anchors.leftMargin: buttonMargin
		anchors.verticalCenter: parent.verticalCenter
		width: buttonWidth
		height: 50

		title: qsTr("New game")
		text_size: 13
		function buttonClick() {
			logic.newGame();
			logic.checkBoard();
		}
	}

	CustomButton {
		id:hintButton
		z:1
		anchors.left: newGameButton.right
		anchors.leftMargin: buttonMargin
		anchors.verticalCenter: parent.verticalCenter
		width: buttonWidth
		height: 50

		property int firstHint: -1
		property int secondHint: -1

		title: qsTr("Get hint")
		text_size: 13
		function buttonClick() {
			if (firstHint >= 0 && secondHint >= 0) {
				view.currentIndex = firstHint;
				view.currentItem.color = "red";
				view.currentIndex = secondHint;
				view.currentItem.color = "red";
			}
		}
	}

	CustomButton {
		id:exitButton
		z:1
		anchors.left: hintButton.right
		anchors.leftMargin: buttonMargin
		anchors.verticalCenter: parent.verticalCenter
		width: buttonWidth
		height: 50

		title: qsTr("Options")
		text_size: 13

		function buttonClick() {
			optionsPrompt.visible = true;
		}
	}

	CustomButton {
		id:optionsButton
		z:1
		anchors.right: parent.right
		anchors.rightMargin: 5
		anchors.left: parent.left
		anchors.leftMargin: 5
		height: 30

		title: qsTr("Exit game")
		text_size: 12

		function buttonClick() {
			dialogOnExit.open();
		}
		MessageDialog {
			id:dialogOnExit
			title: "Exiting..."
			icon: StandardIcon.Question
			text: "Exit game?"
			standardButtons: StandardButton.Yes | StandardButton.No
			onYes: {
				Qt.quit();
			}
		}
	}

	Text {
		id:scoreText
		anchors.right: parent.right
		anchors.rightMargin: 15
		anchors.bottom: parent.bottom
		anchors.bottomMargin: 5
		font.pointSize: 12

		property int totalScore: 0
		text: qsTr("Score: <b>%1</b>".arg(totalScore))
	}
	Text {
		id:movesText
		anchors.left: parent.left
		anchors.leftMargin: 15
		anchors.bottom: parent.bottom
		anchors.bottomMargin: 5
		font.pointSize: 12

		property int totalLeftMoves: 0
		text: qsTr("Moves left: <b>%1</b>".arg(totalLeftMoves))
	}


	MouseArea {
		anchors.fill: parent
	}

	Connections {
		target: logic
		onScoreChanged: scoreText.totalScore = logic.score
		onMovesLeftChanged: movesText.totalLeftMoves = logic.movesLeft
		onHintItems: {
			hintButton.firstHint = first;
			hintButton.secondHint = second;
		}
	}
}
