import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Window {
	id:mainWindow
	visible: true
	title: qsTr("Match 3")
	color: "lightyellow"
	property int cellSide: 60

	width: cellSide * logic.totalCols + 20
	height: cellSide * (logic.totalRows - 1) + 130
	maximumWidth: width
	minimumWidth: width
	maximumHeight: height
	minimumHeight: height

	property int buttonWidth: 90
	property int buttonMargin: (width - buttonWidth * 3) / 4

	TopMenu {
		id:menu
	}

	Rectangle {
		anchors.top: menu.bottom
		anchors.left: parent.left
		anchors.leftMargin: 10
		width: parent.width - 20
		height: parent.height - menu.height - 5
		color: "lightblue"

		GridView {
			id:view
			interactive: false
			model:logic
			property int activeIndex: -2

			anchors.fill: parent
			cellWidth: cellSide
			cellHeight: cellSide
			Component.onCompleted: positionViewAtEnd();

			delegate: Rectangle {
				id:gamePiece
				width: view.cellWidth
				height: view.cellHeight
				color: "transparent"

				property bool deletedFlag: false

				Rectangle {
					anchors.margins: 5
					anchors.fill: parent
					color: (view.activeIndex === index) ? "lightgreen" : "lightyellow"
					radius: 10

					Image {
						anchors.margins: 5
						anchors.fill: (figureImg === -1) ? undefined : parent
						source: (figureImg === -1) ? "" : figureImg

						MouseArea {
							anchors.fill: parent
							onClicked: {
								view.clearHint();
								if (index != view.activeIndex && view.activeIndex >= 0 && !moveAnimation.running) {
									logic.swapElement(index, view.activeIndex);
								}
								if (view.activeIndex >= 0) {
									view.activeIndex = -2;
								}
								else if (!moveAnimation.running) {
									view.activeIndex = index;
								}
							}
						}
					}
				}

				Image {
					id: deleted
					source: "qrc:/images/explotion.png"
					anchors.fill: parent
					anchors.margins: 10
					visible: (deletedFlag) ? true : false
				}
			}
			move: Transition {
				id:moveAnimation
				NumberAnimation {
					properties: "x,y"
					duration: 400
				}
				onRunningChanged: {
					if (!moveAnimation.running) {
						logic.checkBoard();
					}
				}
			}
			moveDisplaced: Transition {
				NumberAnimation {
					properties: "x,y"
					duration: 400
				}
			}
			remove: Transition {
				NumberAnimation {
					property: "opacity"
					to: 0
					duration: 400
				}
			}

			function clearHint()
			{
				for (var i = 0; i < view.count; i++)
				{
					view.currentIndex = i;
					view.currentItem.color = "transparent"
				}
			}
		}
	}

	PromptDialog {
		id:noPossibleMovesPrompt
		CustomButton {
			anchors.centerIn: parent
			title: qsTr("No possible moves")
			text_size: 12

			function buttonClick()
			{
				logic.clearBoard();
				logic.checkBoard();
				noPossibleMovesPrompt.visible = false;
			}
		}
	}

	PromptDialog {
		id:finishedGamePrompt
		property bool gameResult: false

		Rectangle {
			anchors.centerIn: parent
			width: 200
			height: 200
			radius: 10

			Text {
				id: finishedGamePromptText
				anchors.top: parent.top
				anchors.topMargin: 10
				anchors.horizontalCenter: parent.horizontalCenter
				font.pointSize: 15
				color: "black"
				font.bold: true

				text: "Game finished\n   You %1".arg(finishedGamePrompt.gameResult ? "won =)" : "lose =(")
			}

			CustomButton {
				anchors.top: finishedGamePromptText.bottom
				anchors.topMargin: 20
				anchors.horizontalCenter: parent.horizontalCenter

				text_size: 12
				title: qsTr("Start new game")

				function buttonClick()
				{
					logic.newGame();
					logic.checkBoard();
					finishedGamePrompt.visible = false;
				}
			}
		}
	}

	PromptDialog {
		id:optionsPrompt
		anchors.fill: parent

		Rectangle {
			anchors.centerIn: parent
			width: 200
			height: 270
			radius: 10

			Text
			{
				id:mainText
				anchors.top: parent.top
				anchors.topMargin: 10
				anchors.horizontalCenter: parent.horizontalCenter
				width: 150
				text: "Current options"
				font.pointSize: 15
				color: "black"
				font.bold: true
			}

			Row {
				id:widthRow
				anchors.top: mainText.bottom
				anchors.left: parent.left
				anchors.margins: 5
				width: parent.width

				Label {
					width: parent.width / 2
					text: qsTr("Width:")
					anchors.verticalCenter: parent.verticalCenter
				}
				Text {
					width: 70
					text: logic.totalCols
				}
			}


			Row {
				id: heigthRow
				anchors.top: widthRow.bottom
				anchors.left: parent.left
				anchors.margins: 5
				width: parent.width

				Label {
					width: parent.width / 2
					text: qsTr("Height:")
					anchors.verticalCenter: parent.verticalCenter
				}
				Text {
					width: 70
					text: logic.totalRows - 1
				}
			}

			Row {
				id:maxMoves
				anchors.top: heigthRow.bottom
				anchors.left: parent.left
				anchors.margins: 5
				width: parent.width

				Label {
					width: parent.width / 2
					text: qsTr("Max moves:")
					anchors.verticalCenter: parent.verticalCenter
				}
				Text {
					width: 70
					text: logic.maxMoves
				}
			}

			Row {
				id:minScore
				anchors.top: maxMoves.bottom
				anchors.left: parent.left
				anchors.margins: 5
				width: parent.width

				Label {
					width: parent.width / 2
					text: qsTr("Score to win:")
					anchors.verticalCenter: parent.verticalCenter
				}
				Text {
					width: 70
					text: logic.minScore
				}
			}

			Row {
				id:elementTypes
				anchors.top: minScore.bottom
				anchors.left: parent.left
				anchors.margins: 5
				width: parent.width

				Label {
					width: parent.width / 2
					text: qsTr("Type amount:")
					anchors.verticalCenter: parent.verticalCenter
				}
				Text {
					width: 70
					text: logic.typeCount
				}
			}

			CustomButton {
				id:loadFileButton
				anchors.top: elementTypes.bottom
				anchors.topMargin: 20
				anchors.horizontalCenter: parent.horizontalCenter
				width: 150
				height: 40

				text_size: 12
				title: qsTr("Load options file")
				function buttonClick()
				{
					loadOptionsDialog.visible = true;
				}
				FileDialog {
					id: loadOptionsDialog
					title: qsTr("Select options file (.json)")
					selectMultiple: false
					selectFolder: false
					nameFilters: [ "Json files (*.json)" ]
					selectedNameFilter: "Json files (*.json)"
					onAccepted: logic.newOptionsFile(fileUrl.toString().replace("file://", ""));
				}
			}

			CustomButton {
				anchors.top: loadFileButton.bottom
				anchors.topMargin: 10
				anchors.horizontalCenter: parent.horizontalCenter
				width: 150
				height: 40

				text_size: 12
				title: qsTr("Close")

				function buttonClick() {
					optionsPrompt.visible = false;
				}
			}
		}
	}
	Connections {
		target: logic
		onElementDeleted: {
			view.currentIndex = index;
			view.currentItem.deletedFlag = true;
		}
		onGameFinished: {
			finishedGamePrompt.gameResult = (score > minScore) ? true : false;
			finishedGamePrompt.visible = true;
		}
		onMovesEnded: noPossibleMovesPrompt.visible = true
	}
}
