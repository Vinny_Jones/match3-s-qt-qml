import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Rectangle {
	anchors.fill: parent
	z:2
	color: Qt.rgba(1,1,1,0.5)
	visible: false

	MouseArea {
		anchors.fill: parent
	}
}
